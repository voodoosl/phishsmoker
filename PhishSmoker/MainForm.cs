﻿namespace PhishSmoker
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Forms;
    using System.Net;
    using System.IO;
    using System.Threading;

    public partial class MainForm : Form
    {
        private static Object consoleLock = new Object();
        private static Object totalLock = new Object();
        private List<SmokerThread> threads = new List<SmokerThread>();
        private Int64 totalSent = 0;

        private void resetTotal()
        {
            lock (totalLock)
            {
                totalSent = 0;
            }
        }

        private void addToTotal(Int64 amount)
        {
            if (txtDataPosted.InvokeRequired)
            {
                txtDataPosted.Invoke(new Action<Int64>(addToTotal), new object[] { amount });
                return;
            }
            lock (totalLock)
            {
                totalSent += amount;
                txtDataPosted.Text = ((Decimal)(((Decimal)totalSent / (Decimal)1024) / (Decimal)1000)).ToString("#.####");
            }
        }

        public MainForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Event fired when the "Go" button is clicked
        /// </summary>
        private void btnGo_Click(object sender, EventArgs e)
        {
            txtConsole.Clear();
            resetTotal();

            logToConsole("Scanning Phish Website ...");

            String postUrl = txtExplicitUrl.Text;
            if (txtPhishingSite.Text != "http://")
            {
                // Load the phish page, and get the form post url
                postUrl = findPostUrl(txtPhishingSite.Text);
            }
            if (postUrl == "http://")
                postUrl = String.Empty;

            if (String.IsNullOrEmpty(postUrl))
            {
                logToConsole("Can't continue because POST url wasn't found.");
                return;
            }

            Int32 threadCount = 0;
            Int32.TryParse(txtThreads.Text, out threadCount);
            if (threadCount == 0)
            {
                logToConsole("Thread count was invalid. Must be at least 1.");
                return;
            }

            Int32 threadTimeout = 0;
            Int32.TryParse(txtTimeout.Text, out threadTimeout);
            if (threadTimeout == 0)
            {
                txtTimeout.Text = "60000";
                threadTimeout = 60000;
            }

            Int32 payloadSize = 0;
            Int32.TryParse(txtPayloadSize.Text, out payloadSize);
            if (payloadSize == 0)
            {
                txtPayloadSize.Text = "18";
                payloadSize = 18 * 1024;
            }
            else
                payloadSize = payloadSize * 1024; // kb to bytes

            btnGo.Enabled = false;
            txtPhishingSite.Enabled = false;
            txtProxyIp.Enabled = false;
            txtProxyPort.Enabled = false;
            txtThreads.Enabled = false;
            txtTimeout.Enabled = false;
            txtPayloadSize.Enabled = false;

            logToConsole("Timeout " + (threadTimeout / 1000) + " seconds.");
            logToConsole("Starting " + threadCount + " spammer threads ...");
            Thread.Sleep(2000);

            for (var i = 0; i < threadCount; i++)
            {
                var inst = new SmokerThread(i + 1);
                inst.ConsoleEventLog += inst_ConsoleEventLog;
                inst.DataSent += inst_DataSent;
                var t = new Thread(() => inst.Run(txtProxyIp.Text + ":" + txtProxyPort.Text,
                                                  postUrl,
                                                  threadTimeout,
                                                  payloadSize));
                t.Start();
                threads.Add(inst);
            }
        }

        void inst_DataSent(long amount)
        {
            addToTotal(amount);
        }

        void inst_ConsoleEventLog(string status)
        {
            logToConsole(status);
        }

        /// <summary>
        /// Util method to get the url to post to, from the phishing link
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        private String findPostUrl(String url)
        {
            String phishPage = doGet(url);
            if (String.IsNullOrEmpty(phishPage))
                return String.Empty;

            var indexA = phishPage.ToLower().IndexOf("<form");
            if (indexA == -1)
            {
                logToConsole("Couldn't find a form on the page");
                return String.Empty;
            }

            // page contains a form, so extract the form action.
            indexA = phishPage.ToLower().IndexOf("action=", indexA);
            if (indexA == -1)
            {
                logToConsole("Couldn't find a form action");
                return String.Empty;
            }

            String relativeUrl = String.Empty;
            var indexB = phishPage.ToLower().IndexOf("\"", indexA + 8);
            var indexC = phishPage.ToLower().IndexOf("'", indexA + 8);
            var indexD = phishPage.ToLower().IndexOf(" ", indexA + 8);
            if (indexB < indexC && indexB < indexD)
            {
                // ends in "
                relativeUrl = phishPage.Substring(indexA + 8, indexB - (indexA + 8));
            }
            else if (indexC < indexB && indexC < indexD)
            {
                // ends in '
                relativeUrl = phishPage.Substring(indexA + 8, indexC - (indexA + 8));
            }
            else if (indexD < indexB && indexD < indexC)
            {
                // ends in space
                relativeUrl = phishPage.Substring(indexA + 8, indexD - (indexA + 8));
            }

            indexA = url.LastIndexOf('/');
            logToConsole("Post URL: " + url.Substring(0, indexA + 1) + relativeUrl);

            return url.Substring(0, indexA + 1) + relativeUrl;
        }

        /// <summary>
        /// Util method to log to the console, but only keep the last 50 lines.
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        private void logToConsole(String message)
        {
            if (txtConsole.InvokeRequired)
            {
                txtConsole.Invoke(new Action<string>(logToConsole), new object[] { message });
                return;
            }

            var lines = txtConsole.Lines.ToList();
            lines.Insert(0, message);
            if (lines.Count > 50)
                lines = lines.Take(50).ToList();

            txtConsole.Lines = lines.ToArray();
            txtConsole.Refresh();
        }

        private String doGet(String url)
        {
            String result = String.Empty;
            try
            {
                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);

                webRequest.Proxy = new WebProxy(txtProxyIp.Text + ":" + txtProxyPort.Text);
                webRequest.Method = "GET";
                webRequest.ContentType = "text/html";
                webRequest.ContentLength = 0;
                webRequest.Timeout = 2000;
                webRequest.AllowAutoRedirect = false;

                using (HttpWebResponse webResponse = (HttpWebResponse)webRequest.GetResponse())
                {
                    result = new StreamReader(webResponse.GetResponseStream()).ReadToEnd();
                    logToConsole(webResponse.StatusCode.ToString());
                }
            }
            catch (Exception ex)
            {
                logToConsole(ex.Message);
                return String.Empty;
            }
            return result;
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            this.FormClosing += MainForm_FormClosing;
        }

        void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            foreach(var i in threads)
            {
                i.Stop();
            }
        }
    }
}
