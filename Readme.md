﻿Voodoo PhishSmoker
---

We began to come across a lot of phishing websites targeting Second Life users, and developed this software
to break the sites and hopefully prevent some people having their login details and money stolen.

The phishing sites are nearly always the same PHP code base, and are nearly always hosted on altervista.org
or my3gb.com

The fake Second Life login page written in PHP, writes the victims name, password and IP address to a flat HTML page
with a name that varies.

The PhishSmoker will automatically discover the HTML form, and attempt to find avatar names who have been compromised so they
can be contacted, and warned.
Once this has been been completed, it first begins an XSS attack to redirect anyone trying to read the password page to
http://goatse.ch/ and then begins to flood the hacked passwords list with gigabytes of junk data and insults, to 
obfuscate any new hacked accounts, and make the hacked password file problematic to download.

The malicious phishing websites log the IP address of the machine posting the form, so
to protect you and also make IP filtering more difficult, PhishSmoker relies on Tor and Privoxy.

We are not responsible for what you do with this free software, and it may be illegal to use in some countries.
The software should only be used to prevent malicious websites stealing login credentials.

